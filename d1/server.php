<?php

// echo $_POST['description'];
//start the session so that we may use the $_SESSION super global variable
//sessions are ways for browsers to persist/save data short-term
session_start();

class TaskList {
	//method for adding a new task
	public function add($description){
		//create a new task object
		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];

		//check if a $_SESSION['tasks'] exists. If not, create a new one as an empty array
		//$_SESSION is another kind of super global variable like $_GET and $_POST
		//$_GET and $_POST are only created in the front-end/client via forms, whereas $_SESSION can be created anywhere
		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array();
		}

		//push the newTask object to our tasks array
		array_push($_SESSION['tasks'], $newTask);
	}

	//method for updating an existing task
	public function update($id, $description, $isFinished){
		//change the existing task's description to the new description by using its id as the array index
		$_SESSION['tasks'][$id]->description = $description;

		//if the $isFinished parameter is NOT null (meaning the checkbox is ticked), mark the task as finished (true). If it is null, mark it not finished (false)
		$_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
	}

	public function remove($id){
		array_splice($_SESSION['tasks'], $id, 1);
	}

	public function clear(){
		session_destroy();
	}
}

//create a taskList object
$taskList = new TaskList();

//depending on what "action" the client sends (add/update/remove), our $taskList will run the appropriate operation
if($_POST['action'] === 'add'){
	$taskList->add($_POST['description']);
}else if($_POST['action'] === 'update'){
	$taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}else if($_POST['action'] === 'remove'){
	$taskList->remove($_POST['id']);
}else if($_POST['action'] === 'clear'){
	$taskList->clear();
}

//redirect the user back to the client
header('Location: ./index.php');
