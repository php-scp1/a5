<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S5: Client-Server Communication</title>
</head>
<body>
	<?php session_start();

	//print_r($_SESSION['tasks']);
	?>

	<h3>Add Task</h3>

	<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="add"/>
		Description: <input type="text" name="description" required/>
		<button type="submit">Add</button>
	</form>

	<h3>Task List</h3>

	<?php if(isset($_SESSION['tasks'])): ?>
		<?php foreach($_SESSION['tasks'] as $index => $task): ?>
			<div>

				<form method="POST" action="./server.php" style="display:inline-block;">
					<input type="hidden" name="action" value="update"/>
					<input type="hidden" name="id" value="<?= $index; ?>"/>
					<input type="checkbox" name="isFinished" <?= ($task->isFinished) ? 'checked' : null; ?>/>
					<input type="text" name="description" value="<?= $task->description; ?>"/>
					<button type="submit">Update</button>
				</form>

				<form method="POST" action="./server.php" style="display:inline-block;">
					<input type="hidden" name="action" value="remove"/>
					<input type="hidden" name="id" value="<?= $index; ?>"/>
					<button type="submit">Delete</button>
				</form>

			</div>
		<?php endforeach; ?>
	<?php endif; ?>

	</br>

	<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="clear">
		<button type="submit">Clear All Tasks</button>		
	</form>
</body>
</html>
